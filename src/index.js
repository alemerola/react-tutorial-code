import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Main} from "./Components/Main";
import CountrySpecs from "./Components/CountrySpecs";
import {BrowserRouter as Router, Route} from "react-router-dom";

const router = (
    <Router>
        <Route exact path="/" component={Main} />
        <Route path="/country/:name" component={CountrySpecs} />
    </Router>
)

ReactDOM.render(router, document.getElementById('root'));

