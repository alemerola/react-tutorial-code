const headers = {
    "x-rapidapi-host": "restcountries-v1.p.rapidapi.com",
    "x-rapidapi-key": "f413acd9a5msh21fa4a9362db985p1573ccjsnc746a9f0729f"
};

export const allCountries = () => {
    return fetch(`${process.env.REACT_APP_COUNTRIES_ENDPOINT}/all`, {headers})
        .then(res => res.json());
};

export const countryByName = (name) => {
    return fetch(`${process.env.REACT_APP_COUNTRIES_ENDPOINT}/name/${name}`, {headers})
        .then(res => res.json());
};