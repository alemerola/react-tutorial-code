const headers = new Headers(
    {
        "x-rapidapi-host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com",
        "x-rapidapi-key": "f413acd9a5msh21fa4a9362db985p1573ccjsnc746a9f0729f",
        "accept": "application/json"
    }
);

export const randomJoke = () => {
    return fetch(process.env.REACT_APP_CHUCK_ENDPOINT, {headers})
        .then(res => res.json())
        .then(json => json.value);
};