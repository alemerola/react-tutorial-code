import React from 'react';
import chuck from "../../Chuck.jpg";
import {randomJoke} from "../../Shared/ChuckNorrisAPI";

class ChuckNorris extends React.Component {
    constructor(props) {
        super(props);
        this.state = { joke: '' };
    }

    loadJoke() {
        randomJoke().then(joke => this.setState({joke}))
    }

    render() {
        return (
            <div className="chuck">
                <h2>The Chuck Norris jokes generator</h2>
                <div className="row">
                    <div className="column">
                        <img src={chuck} alt="chuck"/>
                    </div>
                    <div className="column">
                        <div className="joke">
                            {this.state.joke}
                        </div>
                    </div>
                </div>
                <button className="chuck-button" onClick={() => this.loadJoke()}>Give me a Chuck Norris joke</button>

            </div>
        );
    }
}

export default ChuckNorris