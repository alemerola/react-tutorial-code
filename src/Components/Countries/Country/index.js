import React from 'react';
import { Link } from "react-router-dom";

export const Country = ({country}) => {
    return (
        <li>
            <Link to={`/country/${country}`}>{country}</Link>
        </li>
    )
};