import React from 'react';
import {allCountries} from "../../Shared/CountriesAPI";
import earth from "../../Earth.jpg";
import {Country} from "./Country";

class Countries extends React.Component {
    constructor(props) {
        super(props);
        this.state = {countries: [], filteredCountries: []}
    }

    componentDidMount() {
        allCountries().then(countries => this.setState({countries}))
    }

    // eslint-disable-next-line no-use-before-define
    filterList(filter = filter.toLowerCase()){
        if(filter === ''){
            this.setState({filteredCountries: []});
            return;
        }
        const filteredCountries =
            this.state.countries
                .map(c => c.name)
                .filter(c => c.toLocaleLowerCase().includes(filter))
                .splice(0, 10);
        this.setState({filteredCountries});
    }

    render() {
        return (
            <div className="countries">
                <h2>Explore your planet!</h2>
                <img src={earth} alt="earth"/>
                <input type="text" onChange={(e) => this.filterList(e.target.value)}/>
                <div className="countries-list">
                    <ul>
                        {this.state.filteredCountries.map((country, i) => <Country key={i} country={country} />)}
                    </ul>
                </div>
            </div>
        );
    }
}

export default Countries