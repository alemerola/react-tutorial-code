import React from 'react';
import {countryByName} from "../../Shared/CountriesAPI";

class CountrySpecs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {country: null}
    }

    componentWillMount() {
        countryByName(this.props.match.params.name)
            .then(country => this.setState({country: country[0]}))
    }

    render() {
        if(!this.state.country) return null;
        const { name, alpha2Code, region, capital, population, nativeName, borders, timezones, currencies, callingCodes } = this.state.country;
        return (
            <div className="country-specs">
                <div className="card">
                    <div className="container">
                        <div className="card-head">
                            <h2>{name} - {alpha2Code}</h2>
                            <div className="flag">
                                <img src={'https://flagpedia.net/data/flags/w580/'+alpha2Code.toLowerCase()+'.png'} alt="flag"/>
                            </div>
                        </div>
                        <div className="card-body">
                            <span className="spec"><b>Region:</b> {region}</span>
                            <span className="spec"><b>Capital:</b> {capital}</span>
                            <span className="spec"><b>Population:</b> {population}</span>
                            <span className="spec"><b>Native Name:</b> {nativeName}</span>
                            <span className="spec"><b>Border:</b> {borders.join(', ')}</span>
                            <span className="spec"><b>Timezones:</b> {timezones.join(', ')}</span>
                            <span className="spec"><b>Currencies:</b> {currencies.join(', ')}</span>
                            <span className="spec"><b>Calling codes:</b> {callingCodes.join(', ')}</span>
                        </div>
                        <div className="card-footer">
                            <button onClick={() => window.history.back()}>Back</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CountrySpecs