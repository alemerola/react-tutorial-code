import React from 'react';
import Countries from "../Countries";
import ChuckNorris from "../ChuckNorris";

export const Main = () => {
    return (
        <div className="App">
            <div className="row">
                <div className="column">
                    <Countries />
                </div>
                <div className="column">
                    <div>
                        <ChuckNorris />
                    </div>
                </div>
            </div>
        </div>
    )
};